# Microservices
Microservices are small autonomous services that work together to form a well defined cohesive system.

#### Principles

##### Do one thing well
> Gather together those things that change for the same reason, and separate those things that change for different reasons.

The smaller the service, the more you maximize the benefits and downsides of Microservice architecture.

#### Advantages
- Deliver software faster
- Embrace newer technologies
- Allows us to react to change much faster
- Resilience, no single point of failure if planned carefully
- Scale, services on demand
- Ease of deployment (each service has its own deployment model)
- Fast rollback of deployment if required

#### Disadvantages
- Slow to start off
- Complexity increases over a Monolith
- 