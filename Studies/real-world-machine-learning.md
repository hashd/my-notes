## Terminology

- **instance/example** - a single object, transaction, observation or record
- **target/label** - Numerical or categorical attribute of interest
- **features** - input attributes used to predict the target, can be numerical or categorical in nature
- **model** - mathemical object describing the relationship between the features and target
- **training data** - set of instances with a known target to be used to fit in an ML model
- **recall** - using a model to predict a target/label
- **supervised machine learning** - machine learning in which, given examples for which the output value is known, the training process infers a function that relates input values to the output.
- **unsupervised machine learning** - machine learning techniques that dont rely on labeled data but try to find hidden structure in unlabeled data.

## Chapter 01
----------------
Machine Learning is a better and more data driven approach to making decisions. 

ML Workflow has five main components:
- Data Preparation [Historical Data - Input]
- Model Building [Modeling - Computation]
- Evaluation [Modeling - Computation]
- Optimization [Modeling - Computation]
- Predictions on new data [Predictions - Output]

Datasets which follow the same structure throughout are termed as *Heterogeneous* and others are termed as *Homogeneous*.

Every machine learning system is about building models and using those models to make predictions. The pseudo code for a machine learning system can be summarized as:

```
trainingData = loadData(trainingDataSource)
model = buildModel(trainingData, target)
newData = loadData(newDataSource)
predictions = model.predict(newData)
```

The pseudo code for the system with model evaluation can be summarized as:

```
data = loadData(trainingDataSource)
trainingData, testingData = split(data)
model = buildModel(trainingData, target)
trueValues = testingData.extractColumn(target)
predictions = model.predict(trainingData)
accuracy = comparePredictions(predictions, trueValues)
```

The role of the ML algorithm is to use the training set to determine how the set of input features can most accurately predict the target variable. The result of this *learning* is encoded in the *model*.

#### Optimizing Model Performance
You can achieve better model accuracy in three ways

- **Tuning the model parameters**: ML algorithms are configured with parameters specific to the underlying algorithm, and the optimal value of these parameters often depends on the type and structure of the data.
- **Selecting a subset of features**: Many ML problems include a large number of features, and the noise from those features can sometimes make it hard for the algorithm to find the real signal in the data, even though they might still be informative on their own.
- **Preprocessing the data**

### Boosting model performance with advanced techniques

#### Data Preprocessing and Feature Engineering
In any problem domain, specific knowledge goes into deciding the data to collect, and this valuable domain knowledge can also be used to extract value from the collected data, in effect adding to the features of the model before model building. This process is termed **Feature Engineering**.

Things like Datetime, Location can be transformed to get even more data which might be of a lot of value to the model.

In many ML systems, you'll have data and predictions flowing into the system and the model improves with time and adapts to changes in the data. This process is termed as **Online Learning**.

## Real World Data
--------------------
With high-quality data, subtle nuances and correlations can be accurately captured and high-fidelity predictive systems can be built. But if training data is of poor quality, the efforts of even the best ML algorithms may be rendered useless. Much of the art of machine learning is in exploring and visualizing training data to assess data quality and guide the learning process.

### Data Collection
The goal of supervised learning is to predict the targets, features which do not have a correlation on the target should be excluded. Useless features make it even more difficult to find true relationships (*signals*) from the random perturbations(*noise*) in the data.

Most practical approach to data collection is:
- include all the features that you suspect to be predictive of the target.  Fit an ML Model. If the accuracy of the model is sufficient, stop.
- otherwise expand the feature set and repeat step 1.
- otherwise starting from the expanded feature set, run a ML feature selection algorithm to choose the best and most predictive features.

These factors determine the amount of training data needed:
- complexity of the problem
- accuracy required in the system
- dimensionality of the feature space

More training data results in higher accuracy because of the data-driven nature of ML models.

### Preprocessing Data for Modeling

#### Categorical Features
A feature is *categorical* if the values can be placed in a bucket and the order of values isn't important.

## Modeling and Prediction
--------------------------------
The purpose of modeling is to accurately estimate the functional relationship between the input features and the target variable. Input features are typically referred to using the symbol **X** with subscripts differentiating inputs when multiple input features exist. The target variable is typically referred to as **Y**

The relationship between the inputs X and target Y can be succinctly described as:

```
Y = f(X) + E
```

The goal of ML modeling is to accurately describe *f* (**signal**) using data. The symbol E is random **noise** in the data that's unrelated to the function *f*.

Machine learning model has 2 main types. They are:
- **parametric** - assumes that *f* takes up a specific function form. Simple, interpretable but less accurate
- **non-parametric** - doesn't make any such assumptions. Less interpretable but more accurate

#### Parametric methods
The simplest example of parametric approach is *linear regression*. In *linear regression*, f is assumed to be a linear combination of the input features.

```
f(X) = c0 + c1 * X1 + c2 * X2 + ....
```

Other example of common parametric methods are:
- Logistic regression
- Polynomial regression
- Linear Discriminant Analysis
- Quadratic Discriminant Analysis
- Parametric mixture models
- naive Bayes

The drawback of parametric approaches is that they make strong assumptions about the true form of the function f.

#### Non-Parametric methods
In non-parametric methods, *f* doesn't take a simple, fixed function form. Instead the form and complexity adapts to the complexity of the data.

A simple form of non-parametric methods is a **classification tree**. Its a series of recursive binary decisions on the input features. 

Other examples of non-parametric methods include:
- k-nearest neighbours
- splines
- kernel smoothing
- neural nets
- generalized additive models
- boosting
- random forests
- support vector machines



