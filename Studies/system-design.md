### Design

#### Scaling

Vertical scaling is limited to current technology and is expensive.
- Multicore Processors
- SATA, SAS and SSD
- RAM

Horizontal scaling is limited to availability of physical space.
- Load balancing - need for orchestrating requests.

#### Replication
- Master-Slave Model

#### Distribution

#### Caching
- Database level caching
- Memcached