# Introduction to Reactive Programming

## Reactive Programming
**Reactive** is the ability to react to a stimuli (event/message) in a timely manner. In **Reactive Programming**, we write code in terms of events rather than order of lines or statements.

In the **Reactive World**, the sequence of events happening over time is known as a event stream or data stream. To react to these events, we need to monitor them. The process of monitoring events is known as **listening** or **subscribing** to events.

Reactive Programming can be called as a programming paradigm which is about programming with asynchronous data streams.

### Reactive Systems
Reactive systems is a set of components which communicate with each other reactively.

Reactive systems have the following features:
- Responsiveness
- Elasticity
- Resilience
- Message Driven

Reactive architecture is the technique or process of designing Reactive systems.

### Event Driven vs Message Driven
Core principle of Reactive Programming is **Event Driven** approach whereas core principle of Reactive Systems is **Message Driven** approach.

Reactive programming gives us the benefits at the component level locally since events are emitted and processed locally. They cannot work across network in a distributed environment.

Reactive systems gives us the benefits at the system level, because messages are processed and communicated across the network in the distributed system.

One can reap the benefits if one uses Reactive Programming along with a Reactive System.

### Benefits of Reactive System with Reactive Programming
- Self-Healing
- Highly available systems
- Loose coupling
- Low Latency
- Maintainability

## Functional Programming

### Principles of Functional Programming
- Pure functions/No side effects
- Immutable data
- Referential Transparency
- Functions are first-class citizens

**Higher Order Functions** are functions that take one or more functions as parameters or returns a functions as their result or do both.

### Benefits of Functional Programming
- Thread-Safe Code
- Type Safety
- Composability
- Declarative Programming

## Functional Reactive Programming
FRP is a new programming paradigm or a new style of programming that uses the RP paradigm to support asynchronous non-blocking data streaming with backpressure and also uses the FP paradigm to utilize its features.

## Reactive Manifesto
Reactive Manifesto is a manifesto that describes how to design and architect Reactive systems according to your needs.

The four traits of a Reactive System
- Responsive
- Scalable
- Resilient
- Message-Driven (v2.0, was Event-Driven in v1.0)

### Message-Driven
The system should depend upon asynchronous message passing between its components to use the benefits of Message-Driven Architecture for free. Though in Reactive Programming, we represent system changes in terms of events, a Reactive system converts them in Messages under the hood. Messages are immutable by nature.

#### Benefits of Message-Driven architecture
- Thread-safe by design
- Loose Coupling
- Location Transparency

### Scalable
Reactive system should be able to support Scale up/out to be able to respond to users in a timely manner. It should also support to Scale down/in to reduce operational costs.

- **Scale Up**: With more load, a reactive system should be able to easily upgrade it with more and more resources.
- **Scale Down**: When the load decreases, a reactive system should be able to easily give up resources.
- **Scale Out**
- **Scale In**

### Resilience
Resilience is the ability to not just be fault-tolerant but also to be able to self-heal in case of failure.

Resilience means a reactive system must be able to respond to the users even in the event of failures, by recovering itself. In a reactive system, all failures are converted to messages and are then processed.

### Responsive
Responsiveness means to react to an event/message in a timely manner.

## More on Reactive Systems

### Reactive Systems vs Traditional Systems
In Reactive Systems, we focus on the flow of Control whereas in Traditional Systems, we focus on flow of Data.

### Java 9 Flow API
The API primarily consists of the following components:
- **Publisher**: Publisher is a component that acts as the source of data, it emits data.
- **Subscriber**: Subscriber is a component that works as the consumer of data.
- Subscription
- Processor
- Flow

### Marble Diagram
A Marble Diagram is a diagram used to visualize an FRP data transformation in a very nice and easy to understand form.

**Data Transformation**: A data transformation is an operation, which is applied on a set of source data elements or all data elements in a data stream and produces resultant data elements to send to another data stream.

#### Benefits of Marble Diagram
- Represent a simple or complex FRP operation
- Easy to understand how to compose and use an FRP operation
- Easy to design source, data streams and destinations

#### Rules of Marble Diagrams
- A horizontal line represents a data stream
- Some symbols like circles, diamonds and rectangles represent data elements in the data stream
- Big Rectangular boxes represent the actual FRP operation or the data transformation stage
- Vertical line on top of the horizontal line represents data stream completing successfully
- Cross mark on top of the horizontal line represents data stream completing with an error

#### Important FRP operators
- `map`
- `flatmap`
- `merge`
- `filter`
- `reduce`

### Observer Pattern vs Reactive Pattern
Observer pattern is a widely used OOP design pattern with 2 main elements:
- Subject (Observable)
- Object (Observer)

Reactive pattern is more than a Observer pattern. It is a combination of Observer, Iterator and FP patterns.

# Functional Scala
Unlike Java, Scala is a pure OO and Functional language. Java is not pure OOP since it supports static methods and value types. Scala supports strongly typed systems.

Scala supports FP features like:
- Pure Functions
- Immutable Data
- Referential Transparency
- First-class citizen functions
- Anonymous functions
- Higher Order functions
- Currying
- Tail Recursion
- Implicit
- Type Classes

Scala also supports the following FP Design Patterns:
- Monoid
- Monad
- Functor

## Functional Features of Scala

### Immutability
Immutable data is one we cant modify once created.

- In Scala, Function arguments are defined as `val` by default
- In Scala, most of the data is defined as `case class` which are immutable

#### Benefits of Immutability
- No Side Effects
- Easy to reason about and test
- Thread Safety
- Easy to write Concurrent and parallel programs

### Pure Functions
Functions whose result only depends on its inputs. Irrespective of how many times the function is executed with the same set of inputs, it should provide the same result.

**Combinator** is a *Pure Function* and also a *Higher Order Function*.

If a function doesn't return a value, that function will most likely have some side-effect. It's not recommended to work with such functions in the FP world.

### Anonymous Functions
A function without a name, also known as a function literal. It works like a normal function but can't be execute with a name because it doesn't have one. It can however be assigned to a variable and be executed.

### Referential Transparency
An expression or a function call maybe replaced by its value, without changing the behaviour of the application.

### Functions are First Class Citizens
Functions can be treated as values. So the following would be possible:
  - Functions can be assigned to variables
  - Functions can be passed as parameters for function calls
  - Functions can be returned as a result from function calls

### Partial Functions
A function that does not provide a result for every possible input it can be given.

### Currying
Function currying is sort of the process of converting a single parameter list into multiple parameters list.

### Higher Order Functions
Higher Order Functions are functions that:
- Either accept one or more functions as parameters
- Returns a function as result
- Satisfy all of the above

### Type Class
A Type Class defines some behaviour in terms of operations. If a new class wants to be a type of the Type Class, it needs to provide implementations to all operations which are defined in that Type Class.

> Note: In Scala, any operator ending with a `:` is right associative.
> `numbers = 4 :: numbers` is equivalent of `numbers = numbers.::(4)` instead of `4.::(numbers)`

## Functional Design Patterns in Scala

### Monoid
In Scala, Monoid is a Type Class or Data Structure with the following 2 rules:
- **Associative Rule**: `A1 op (A2 op A3) === (A1 op A2) op A3`
- **Identity Rule**:

### Functors
In Scala, Functor is a Type Class or Data Structure which contains a map method with the following signature:

``` scala
def map[A, B](f: A => B): C[A] => C[B]
```

### Monad
In Scala, Monad is a Type Class or Data Structure with the following functions:
- `apply`: which creates a instance of the Type Class
- `flatmap` and `map`: which transforms the monad of type S to type T

``` scala
trait MyMonad[T] {
  def map[T](f: T => S): MyMonad[S]
  def flatmap[T](f: T => MyMonad[S]): MyMonad[S]
}

object MyMonad[T] {
  def apply[T](t: T): MyMonad[T]
}
```
