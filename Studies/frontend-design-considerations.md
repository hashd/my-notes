#### Architecture Considerations
- Load Balancing
- CDN
- Caching
- Containerization
- Analytics
- Logging

#### Server Considerations
- Latency
- Geolocation

#### Framework Considerations
- Routing
- API Handling - client-side