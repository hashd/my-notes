### Arrays
---------------
- ARR-001: Given an array of size n, the goal is to find out the smallest number that is repeated exactly ‘k’ times where k > 0?
- ARR-002: Given an array of size n containing elements from 1 to m, the goal is to find out the smallest number that is repeated exactly ‘k’ times where k > 0 and k < m?
- ARR-003: Given an array, print k largest elements from the array.  The output elements should be printed in decreasing order.
- 

### Linked Lists
---------------
- LLT-001: Given a linked list, write a function to reverse every k nodes (where k is an input to the function).If a linked list is given as 1->2->3->4->5->6->7->8->NULL and k = 3 then output will be 3->2->1->6->5->4->8->7->NULL.
- LLT-002: Given two numbers represented by two lists, write a function that returns sum list. The sum list is list representation of addition of two input numbers.

### Stacks
----------------
- STK-001: Design a data-structure SpecialStack (using the STL of stack) that supports all the stack operations like push(), pop(), isEmpty(), isFull() and an additional operation getMin() which should return minimum element from the SpecialStack. Your task is to complete all the functions, using stack data-Structure.
- 