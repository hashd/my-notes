# Scalability

## Core Concepts
Scalability is an ability to adjust the capacity of a system cost-efficiently to fulfill increasing demands.

Ability to scale is measured in different dimensions.

- Handling more data
- Handling higher concurrency levels
  + Limited amount of CPUs and execution threads
  + Synchronization of parallel code execution to ensure consistency
- Handling higher interaction rates (Latency)

Scalability is related to performance but its not the same thing. Performance measures how much time it takes for a task to complete whereas scalability tells us how much we can grow or shrink.

> VPS is a term used by hosting providers to describe a virtual machine for rent. it is hosted together with other VPS instances on a shared host machine. They are cheap and can be upgraded instantly.

> Cache is a server/service focused on reducing the latency and resources needed to generate the result by serving previously generated content. Caching is a important technique for scalability.

Scalability is necessary to handle
- increase in user base
- increase in data persisted
- more CPU intensive features that might get added in the future

Two types of scalability:
- Vertical
- Horizontal

### Vertical Scalability
`Vertical scalability` is easily achieved by upgrading the hardware or network throughput. It does not require any architectural changes to the application.

There are number of ways to achieve **vertical scalability**:

- Adding more I/O capacity by adding more HDD in RAID arrays. RAID 10 has become especially popular, as it gives both redundancy and increased throughput.
- Improving I/O access times by switching to SSDs.
- Reducing I/O operations by increasing RAM. More memory means bigger cache and more memory for the applications.
- Improving network throughput by upgrading network interfaces or installing additional ones.
- Switching to servers with more processors or more virtual cores. Even OS context switches are drastically reduced with more processors/cores.

Limitations:

- Cost. Cost of components shoot up crazily after a certain point.
- Hard Limits. Hardware might not be available in some cases due to physical constraints or technology advancements.
- Operating system design might limit the use of hardware.
- Application architecture might limit the use of hardware.

### Isolation of Services
Another simple solution for improved scalability is to separate the different parts of a system. A service is an application like a *web server* or a *database* engine.

Isolating services from a single server setup to separate servers is just a slight evolution. It is however a great next step since one can distribute the load among more machines than before and scale each of them vertically as needed.

> The process of dividing a system based on functionality to scale it independently is called functional partitioning.

### Content Delivery Networks: Scalability for static content
As application grows, it becomes beneficial to offload some traffic to a third-party content delivery network (**CDN**) service.

> A CDN is a hosted service that takes care of global distribution of static files like images, JavaScript, CSS, and videos. It works as an HTTP proxy. Clients that need to download images, JavaScript, CSS, or videos connect to one of the servers owned by the CDN provider instead of your servers. If the CDN server does not have the requested content yet, it asks your server for it and caches it from then on. Once the file is cached by the CDN, subsequent clients are served without contacting your servers at all.

Users can benefit from better resource locality as CDN servers are usually global companies with data centers located all over the world.

Relying on the third-party service's ability to scale one would be able to offload some of the load of the application servers.

### Horizontal Scalability
> It is accomplished by a number of methods to allow increased capacity by adding more servers. It is considered to be the holy grail of scalability.

It is much harder to achieve and in most cases, it has to be considered before the application is built. It might require a significant development effort to introduce horizon scalability at a much later stage of development.

Systems that are truly horizontally scalable do not need **strong servers**, quite the opposite; they usually run on lots and lots of cheap *commodity* servers.

Initial costs of horizontal scalability might be more than vertical scalability but over time and as the application begins to have more load, horizontal scalability triumphs other alternatives hands down. Using horizontal scalability, you avoid the high prices of top-tier hardware and you also avoid hitting the vertical scalability ceiling.

Each server role in the system can be scaled up by adding more servers in a horizontal scalable system. But it is difficult to achieve it in certain server roles than others, like database/persistence store.

> Round Robin DNS is one approach to distribute traffic across multiple servers. Round-robin DNS is a DNS server feature allowing you to resolve a single domain name to one of many IP addresses.

#### Scaling for a global audience
Once we start serving more than a million users, we'll need to spread our servers across the globe to provide a much better experience and to be failsafe. Scaling for a global audience by spreading the servers across the globe requires a few more tricks and poses a few more challenges.

> GeoDNS is a DNS Service that allows domain names to be resolved to IP addresses based on the location of the customer. The goal is to direct the customer to the closest server  geographically to minimize network latency.

Another extension of the infrastructure is to host multiple edge-cache servers located around the world to reduce the network latency further. Edge-cache servers are most efficient when they act as simple reverse proxy servers caching entire pages, but they can be extended to provide other services as well.

#### Overview of a Data Center Infrastructure
A reference data center might look something like

```
									Customer
										|
					DNS/geoDNS	-	Internet - CDN (Content Delivery Networks)
										|
----------------------------------------------------------------------------------
								Load Balancers
										|
								Front Cache Servers
										|
			------------------Front App Servers---------------
	Cache Servers						|		               Message Queue Servers
			------------------Web Services Server-------------      |
										|                              |
					----------------------------------       Queue Worker Machines					|							        |
			Search Servers						Data Store Servers

-----------------------------------------------------------------------------------
```

**Load Balancer**: A load balancer is a software or hardware component that distributes traffic coming to a single IP address over multiple servers. They let us dynamically add or remove servers.

Web traffic usually goes through the following phases/servers/hardware:

- DNS/geoDNS Lookup
- Load Balancer
- Front Cache Servers (optional: to cache frontend server stuff)
- Frontend Web Servers (CsR)/Web application servers(BsR)
- Web Services Layer
- Cache Servers (leveraged by Frontend and Web Services layer)
- Message Queues (leveraged by Frontend and Web Services layer)
- Data Layer (mono or polyglot persistence)

As long as we keep a certain layer, stateless, it is easy to scale horizontally by adding more servers.

Having more components may be more exciting, but it makes releases, maintenance and recovery procedures much more difficult.

#### Overview of the Application Architecture
Application architecture should not revolve around a framework or a particular technology. Architecture should evolve around the business model. Without the right model and the right business logic, our databases, message queues, and web frameworks are useless.

> A domain model is created to represent the core functionality of the application in the words of business people, not technical people.

##### Frontend
Frontend applications will have to developed in such a way that will allow communication over HTTP, including AJAX and web sessions. By hiding that within the frontend layer, the web services layer can simply focus on the business logic not on the presentation layer and web specific technologies.

Frontend should not be aware of any databases or third party services. Projects that allow business logic in the frontend code suffer from low code reuse and high complexity.

Frontend components are allowed to send events to message queues and use cache backends as they serve a major purpose in scaling out. Whenever we can cache an entire HTML or HTML fragment, we save much more processing time than caching just the database query that was used to render the HTML.

#### Web Services
Web Services are the place where most of the business logic should live.

##### Service oriented Architecture
> **Service Oriented Architecture (SOA)** is architecture centered on loosely coupled and highly autonomous services focused on solving business needs. In SOA, it is preferred that all the services have clearly defined contracts and use the same communication protocol. SOA is an architectural style and SOAP is a set of technologies used to define, discover and use web services.

> **SOAP** - Simple Object Access Protocol

##### Multilayered Architecture
> **Multilayered Architecture** is a way to divide functionality into sets of layers. Components in the lower level expose an API which can be consumed by clients residing in the layers above but never vice versa.

Layers enforce structure and reduce coupling as components in the lower layers become simpler and less coupled with the rest of the system. It is easy to replace the lower layers if the new components fulfill the API.

Changes in upper layer are easier to make since less components depend on them. It is expensive to make changes in the lower layers.

##### Hexagonal Architecture
> **Hexagonal Architecture** assumes that the business logic is in the center of the architecture and all interactions with the data store, clients and other systems are equal. There is a contract between the business logic and every non business logic component but there is no distinction between the layers above and below.

##### Event Driven Architecture
> **Event Driven Architecture** is about reacting to events that have happened. Traditional architecture is about responding to requests and requesting work to be done. In Event Driven model, we don't wait for things to be done.

No matter the actual architecture style of the system, all architectures will provide a benefit from being divided into smaller independent functional units. The purpose is to build higher abstractions that hide complexity, limit dependencies, allow each part to scale independently and make parallel development possible.

#### Supporting technologies
Things like message queues, application cache, main data store and search engine are isolated since they are usually implemented in different technologies and most often they are third party technologies. They can be treated as black boxes in the context of the architecture. They can be considered as plug and play extensions to the overall architecture.

**Architecture** is the perspective of the software designer; **Infrastructure** is the perspective of the system engineer; Each perspective shows a different view at the same problem, ie. to build scalable software.

## Principles of Good Software Design
Software design principles are more abstract and more general than scalability itself, but they lay down a solid foundation for building scalable software.

### Simplicity
Keeping software simple is difficult because it is inherently relative. There are four basic steps to promote simplicity:

#### Hide Complexity and Build Abstractions
As the system grows, there will be too many details to keep in the mental model. Either you see the whole system without much detail or you see all the details of a narrow part fo the system. To make software simple is to allow a mental zoom in and zoom out. As system grows, it cannot and will not be simple so we need to thrive for local simplicity.

To build local simplicity, one needs to separate functionality into modules.

#### Avoid Overengineering
Building a solution that is much more complex than what is really necessary makes the system much tougher to maintain and handle. Beginning with a reasonable level of abstraction and iterating over it gives better results than trying to predict the future.

Building software that is simple to understand and proves to be extensible can give a great deal of satisfaction.

#### Try test-driven development
> **Test Driven Development** is a set of practices where engineer writes tests first and then implement the actual implementation.

##### Benefits of TDD:
- No code without unit tests, thus no *spare* code
- No unnecessary functionality, as it'll require tests as well
- Tests can be used as documentation for the functionality

The difference in approach over traditional methods results in greatly improved code design and application programming interface (API).

#### Learn from Models of Simplicity in Software Design
When things falls into place naturally, when there is no difficulty in adapting the system or understanding it, well crafted simplicity has been achieved.

### Loose Coupling
Second most important principle of software design is to keep coupling as low as possible.

> Coupling is a measure of how much two components know about and depend on each other.

##### Effects of low and high coupling:
- High coupling means changing a single line of code might have a higher chance at introducing bugs than a lowly coupled system.
- Low coupling promotes localized complexity and thus multiple engineers can work on different pieces independently.
- Decoupling on a higher level can mean having multiple applications with each one focused on a narrow functionality.

#### Promoting Loose Coupling
One needs to carefully manage dependencies between modules, classes and applications.

> A system is the whole thing: it encompasses all of the **applications** you develop and all the **software** you use in your environments.

Hiding details of the internal implementation is a great way to promote loose coupling and simplicity. To reduce coupling on the higher levels of abstraction, you want to reduce the contact surface area between parts of your system.

#### Avoid Unnecessary coupling
- Creating a new class should not begin by adding public getters and setters for all members of the class.	This breaks encapsulation and invites coupling. **The point is to hide as much as possible and expose as much as necessary.**
- Clients of a module/class should not have to invoke methods or APIs in a particular order for the work to be done correctly.
- Circular dependencies between layers of applications, modules, or classes is a bad coupling practice.

A diagram of a well designed module should look more like a DAG rather than a social network graph.

### Coding to Contract
It is primarily about decoupling clients from providers.

> A **contract** is a set of rules that provider of the functionality agrees to fulfill.

Contract means different things in different contexts:

- Signature of the method, in context of methods
- Public interface, in context of classes
- Publicly available classes and interfaces, in context of modules
- Web Service API specification, in context of applications

When designing code, create explicit contracts wherever possible and depend on them instead of the implementations.

### Draw Diagrams
Diagrams summarize knowledge and they truly are worth a thousand words.

> Don’t get discouraged if you find it difficult to design up front. It is not an easy task to switch from code first to design first, so be prepared that it may take months or years before you get completely comfortable with the process.

#### Types of Diagrams

##### Use Case diagram
A use case diagram is a simplified map defining who the users of the system are and what operations they need to perform.

##### Class diagram
Class diagrams are the best tools to visualize a module’s structure with its classes, interfaces, and their interdependencies.

##### Module diagram
You use module diagrams to show high-level dependencies and interactions between a module and its direct neighbors of the dependency graph. A module can be a package or any logical part of the application responsible for a certain functionality.

### Single Responsibility
Classes should have one responsibility and no more. It reduces coupling, increases simplicity and makes it easier to refactor, reuse and test.

##### Guidelines to build Single Responsibility
- Keep class length to less than 2-4 screens of code
- Ensure that classes depend on no more than 5 interfaces/classes
- Ensure that a class has a specific goal/purpose
- Summarize the purpose of the class and put it as a comment on top of the class

### Open Closed Principle
Any time we create code with the intent to expand it in the future without the need to modify it, we say that we've applied open-closed principle. It increases flexibility and makes future changes cheaper.

### Dependency Injection
Dependency injection provides references to objects that the class depends on, instead of allowing the class to gather the dependencies itself. It seems like a subtle change from pull to push, but it has a great impact on the flexibility of software design.

### Inversion of Control
Dependency injection is limited to object creation and assembly of its dependencies. Inversion of control, on the other hand, is a more generic idea and can be applied to different problems on different levels of abstraction.

> Inversion of control (IOC) is a method of removing responsibilities from a class to make it simpler and less coupled to the rest of the system. It is about making a class as dumb as possible.

### Designing for Scale
> 90% of all startups fail as per history, 9% of the rest need limited scalability and need not worry about it. Only about 1% of startups require horizontal scalability.

Scalability solutions can be boiled down to 3 techniques:

- Adding more clones
- Functional partitioning
- Data partitioning

#### Adding more clones
A clone is an exact replica of a component or a server. Each of them must be equally qualified to serve an incoming request.

Scaling by adding clones works best for stateless services, as there is no state to synchronize. If your web services are stateless, then a brand new server is as good as a server that is already serving requests. It is the easiest and cheapest technique to implement in your web layer.

It is difficult to scale by adding more clones when servers are stateful.

#### Functional Partitioning
Parts of the system should be focused on a specific functionality.

In the context of infrastructure, functional partitioning is isolation of different server roles like:

- Object Cache servers
- Message Queue servers
- Queue Workers
- Web Servers
- Data Store Engines
- Load Balancers

In a more advanced form, functional partitioning is dividing a system into self-sufficient applications. It is applied most often in the web services layer, and it is one of the key practices of service-oriented architecture (SOA).

Additional benefits of such division is the ability to have multiple teams working in parallel on independent codebases and gaining more flexibility in scaling each service as different services have different scalability needs.

##### Drawbacks
- Requires more management and effort to start off with
- Limited number of functional partitions

#### Data Partitioning
Data is partitioned such that each machine manages its own data. This is a manifestation of a share-nothing principle. Share nothing is an architectural principle where each node is fully autonomous.

Not sharing data/state means

- No data synchronization
- No need for locking
- Failures can be isolated

Data partitioning, applied correctly with scaling by adding clones, effectively allows for endless scalability. Unfortunately, *data partitioning is the most complex and expensive technique.*.

### Design for Self-Healing
A system is considered to be available as long as it performs its function as clients expect it to.

> A highly available system is a system that is expected to be available to its clients most of the time.

The larger the system gets, the higher the chance of failure. Operating at scale magnifies the chance even more that we must consider failure a norm and not a special condition. Preparing for the worst is a good thing as we scale out.

Continuously testing different failure scenarios is a great way to improve the resilience of your system and promote high availability. Ensuring high availability is all about reducing Single Point of Failures and graceful Failover.

> *Single Point of Failure* is any piece of infrastructure that is necessary for the system to work properly.

> **Redundancy** is having more than one copy of each piece of data or component of the infrastructure.

Systems that are not redundant need special attention, and it is a best practice to prepare a disaster recovery plan with recovery procedures for all critical pieces of infrastructure.

**Self-healing** is a property beyond graceful failure handling; it is the ability to detect and fix problems automatically without human intervention.

Software Engineering is all about making informed decisions, creating value for business and preparing for the future.

## Building the Front-End Layer
Every user interaction, connection and response has to go through the frontend layer so it demands the highest throughput, concurrency rates making its scalability critical. Luckily, a well designed front-end scales relatively easily and caching gives the highest returns.

Frontend that scale are mostly

- stateless
- demand on caching heavily
- allow horizontal scalability by simply adding more hardware

Today's web applications are either tranditional multipage web applications or single page applications (SPA) or Hybrid of the previous two.

### Managing State
> **Statelessness** is a property of a service, server or object indicating that it does not hold any state.

In the context of stateful vs stateless, state is any information that would have to be synchronized between servers to make them identical.

#### Managing HTTP Sessions
Since HTTP protocol is stateless, web applications developed techniques so that servers could recognize multiple requests from the same user as parts of a more complex and longer lasting sequence ie User Session. They are typically implemented using cookies.

Cookies allow the web server to figure out which requests belong to a particular user. This in turn allows implementation of user login functionality and other similar features.

Web session scope has to be stored outside of the web server so that any other web server can also handle following requests. There are 3 common ways to solve this problem:

- Store session state in cookies
- Delegate the session storage to an external data store
- Use a load balancer that supports sticky sessions

Storing a session in a cookie is fairly simple and involves serializing the session data, encrypting and including it in the response header as a new value of the session data cookie. Main advantage in this approach is that the session state doesn't have to be persisted anywhere in the data center.

Drawbacks of using cookies:

- Limited size of cookies
- Cookies are sent as part of every request

Second approach is to store the session data in a dedicated session store. In this case, the web application would take the session identifier from the web request and then load the session data from an external data store.

Third approach is to use the load balancer to redirect the request to the server that initially issued the session cookie. Load Balancer itself can issue a cookie to the request.

> Sticky sessions break the fundamental principle of statelessness. It is recommended to avoid them. Recommened approach is to use Cookies or a shared Object Store accessible from all web server instances.

#### Managing Files
There are two types of files to pay attention to:

- User generated content being uploaded to servers
- Files generated by the system that need to be downloaded by the user

Each of these use cases may require files to be publicly or privately available to a selected users.

Publicly available files should most likely be served over a CDN irrespective of where they are stored. By setting a long expiration policy on public files, we can let CDN cache them forever.

For the private content, the request will have to go through frontend web application servers rather than being served directly by the CDN.

Building a simple file storage is relatively easy, but making it scalable and highly available is a much more complex task. It is better to use an *out of the box* open source data store to store the files.

> Distributed file storage is a complex problem, so if possible stick with a third-party provider like S3, Azure first.

*Prefer a shared and distributed cache server over local cache servers for the frontend application servers.*

### Components of a Scalable Front-End
- Web Servers
- Load Balancers
- Domain Name System (DNS)
- Reverse Proxies
- CDN

#### DNS
DNS is used to resolve domain names to IP addresses. Using a third party DNS service is a better option over hosting one on your own.

Using round robin DNS to distribute traffic directly to web servers makes server management and failure recovery much more complicated and should not be used in production.

#### Load Balancers
Having a load balancer handle requests hides the structure of the data center and current server responsibilities.

##### Benefits of using Load Balancers
- Hidden server maintenance
- Seamlessly increase capacity
- Efficient failure management
- Auto scaling
- Effective resource management

##### Load Balancers as a Hosted Service
Advantages of using Elastic Load Balancer by Amazon:

- Scales transparently
- Built-in High Availability
- Cost effective with minimal up-front costs
- Enables auto scaling
- SSL offloading
- Supports graceful back-end server termination
- Amazon AWS SDK Support

##### Self Managed Software-Based Load Balancer
A reverse proxy like **Nginx** or a specialized load balancer software like **HAProxy** can be leveraged.

HAProxy can be configured as a Layer 4 or Layer 7 Load balancer. Layer 4 enables it to act as load balancer for not just HTTP/HTTPS requests whereas Layer 7 proxy supports sticky sessions and SSL termination.

##### Hardware Load Balancer
Hardware load balancers offer high throughput, extremely low latencies and consistent performance at a significant cost. Hardware load balancers need specialized training to operate and such skills can be rare to find.

#### Web Servers
As long as your front-end web servers are truly stateless, you can always scale out horizontally by simply adding more servers.

#### Caching
Caching helps to avoid requests being served by the front-end servers.

##### Ways to cache content
- Using CDN to direct web requests to the web servers
- Using reverse proxies in data center like Varnish and Nginx
- Caching content in the browser using WebStorage or LocalStorage
- Use a shared object cache like Redis or Memcached

#### Auto Scaling
Scalability is not just about scaling out, but also about scaling down as necessary mainly to cut costs.

## Web Services
Web Services is the place where most of the business logic lives should you choose to use them.

##### Advantages of Web Services
- Promotes reuse
- Higher levels of abstraction

##### Drawbacks
- Higher up-front development costs
- Increased complexity

Arguably the oldest approach to developing web services in the context of web applications is to build the web application first and then add web services as an alternative interface to it.

Not every web application needs an API, and designing every web application with a distinct web services layer may be just unnecessary overengineering.

Monolithic approach is the worst option from a scalability and long-term maintenance point of view.

### API First Approach
It implies designing and building the API contract first and then go about building the web service and the clients. It came about a solution to multiple user interfaces.

Pushing the business logic into the web services layer, makes developing and changing clients easier. The web service layer can be developed without worrying about the how the clients are implemented and the clients can be implemented without worrying about the internals of the web service layer.

API first should not be mantra; some applications might benefit from it; some might not.

### Types of Web Services
Two main architectural types of web services:

- Function-centric services
- TODO

#### Function Centric Services
The concept of this approach is to be able to invoke *functions or object methods* on  remote machines.

##### Types of Function Centric Technologies
- CORBA
- XML-RPC
- DCOM
- SOAP

SOAP had gained the dominant traction among these due to backing of Industry Leaders. The most common implementation of SOAP is to use XML to describe and encode messages, and the HTTP protocol to transport requests and responses between clients and servers. One of the features was SOAP was web service discovery and to be able to generate integration code based on contract descriptor themselves.

Web Service providers expose a set of XML resources such as Web Service Definition Language(**WSDL**) describing methods and endpoints available and definition of data structures being exchanged using XML Schema Definiton (**XSD**).

##### Issues with SOAP
- Integration with dynamically typed languages like ruby, python, php and so on.
- Requests can't be cached at the HTTP Level since the parameters and methods names are present the XML document.
- Some of the extension specifications require it to be **stateful**.

#### Resource Centric Services
As the name suggests, this approach focuses around the concept of **resource**. Each resource can be treated as a type of Object and there are only a handful of operations that can be performed on these objects like CREATE/DELETE/UPDATE/FETCH.

It has become the defacto standard of web application integration due to its simplicity and lightweight development model. REST services use URLs to uniquely identify resources.

HTTP methods are used in the following manner:

- **GET** - fetches a resource
- **PUT** - replaces a resource entirely
- **POST** - creates or updates an resource
- **DELETE** - deletes a resource

REST Services do not have to use JSON, but due to its lightweight and simple nature JSON are the defacto standard on the web to exchange resource data.

##### Drawbacks of REST
- No web service discovery
- Clients will not be able to auto generate code
- Less sophisticated security over SOAP

##### Advantages of REST
- Stateless
- Cacheable
- Simple and Lightweight

#### Scaling REST Web Services

##### Stateless Service Machines
All shared state needs to be pushed out of the web service into shared data stores like Object caches, databases and message queues.

Advantages of making Servers stateless

- Traffic can be distributed to all web service machines
- Plug and play mechanism for web services servers
- Easy decommissionning of web services servers
- Easy maintenance and zero downtime for upgrades
- Easy auto-scaling

The only type of state that is safe to keep on a web service server is a object cache as they dont need to be synchronized or replicated.

`TODO: Locking, Transactions`

##### Caching Service Responses
To take advantage of HTTP caching, you need to make sure that all of your GET method handlers are truly read-only. A GET request to any resource should not cause any state changes or data updates.

HTTP Caching would need to include authentication headers when building the caching key if the response of a REST API is different for a different authenticated users. This can be enabled using a **Vary: Authorization** HTTP header if Authoization is the header containing the authentication detail.

To truly leverage HTTP caching, we need to make as many resources public as possible.

To be able to scale using cache, we would deploy reverse proxies between clients and web services.

#### Functional Partitioning
In the context of web services, functional partitioning is a way to split a service into a set of smaller, fairly independent web services, where each web service focuses on a subset of functionality of the overall system.

## Data Layer

### Scaling with MySQL

#### Replication
Replication usually refers to a mechanism that allows us to have multiple copies of the same data stored on different machines.
