The OAuth 2.0 authorization framework enables a third-party application to obtain limited access to an HTTP service, either on behalf of a resource owner by orchestrating an approval interaction between the resource owner and the HTTP service, or by allowing the third-party application to obtain access on its own behalf.

- **Client** - Application Server
- **Resource Owner** - End User
- **Protected Resource/Authorization Server** - Third Party Authorization Server

## Delegating Access
OAuth, the end user delegates some part of their authority to access the protected resource to the client application to act on their behalf. To make that happen, OAuth introduces another component into the system: the authorization server. The authorization server (AS) is trusted by the protected resource to issue special purpose security credentials called OAuth access tokens to clients.

### OAuth Process
- client first sends the resource owner to the authorization server in order to request that the resource owner authorize this client
- resource owner authenti- cates to the authorization server and is generally presented with a choice of whether to authorize the client making the request
- client is able to ask for a subset of functionality, or scopes, which the resource owner may be able to further diminish
- once the authorization grant has been made, the client can then request an access token from the authorization server. This access token can be used at the protected resource to access the API, as granted by the resource owner


